<?php

namespace app\models;

use yii\base\Model;

/**
 * Class UploadFileForm
 * @package app\models
 */
class UploadFileForm extends Model
{
    /**
     * @var UploadedFile атрибуты файла
     */
    public $file;

    /**
     * @return array правила валидации.
     */
    public function rules()
    {
        return [
            [['file'], 'file', 'extensions' => 'txt'],
        ];
    }
}