<?php

namespace app\models;

use app\core\services\TextParse;
use Yii;
use yii\db\ActiveRecord;
use yii\db\Exception;

/**
 * Модель таблицы "file_info".
 *
 * @property int $id
 * @property string|null $created_at
 * @property string|null $file_name
 *
 * @property FileWordsCounts[] $fileWordsCounts
 */
class FileInfo extends ActiveRecord
{
    const PAGE_LIMIT = 5;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'file_info';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['created_at'], 'safe'],
            [['file_name'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'Идентификатор',
            'created_at' => 'Время создания',
            'file_name' => 'Название файла',
        ];
    }

    /**
     * Получаение запроса [[FileWordsCounts]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getFileWordsCounts()
    {
        return $this->hasMany(FileWordsCounts::className(), ['file_id' => 'id'])->orderBy([
            'count' => SORT_ASC,
            'word' => SORT_ASC,
        ]);
    }

    /**
     * Запись файла с вхождением слов и количеством
     *
     * @param string $file_name
     * @param TextParse $text_from_file
     * @return FileInfo
     */
    public function saveData(string $file_name, TextParse $text_from_file): FileInfo
    {
        $connection = Yii::$app->db;
        $transaction = $connection->beginTransaction();
        try {
            $this->file_name = $file_name;
            $this->created_at = date('Y-m-d H:i:s');
            $this->save();

            $data_rows = $this->generateRows($text_from_file, $this->id);
            $file_words_model = new FileWordsCounts();
            $connection->createCommand()->batchInsert(
                $file_words_model::tableName(), $file_words_model->attributes(true), $data_rows)->execute();
            $transaction->commit();
            // Ловим все что можно, да это путь в ад и базовы лучший не ловить.
        } catch (Exception $e) {
            $transaction->rollBack();
        }

        return $this;
    }

    /**
     * Генерация массива значений для записи в модель
     *
     * @param TextParse $text_from_file
     * @param int $file_id
     * @return array - список слов
     */
    private function generateRows(TextParse $text_from_file, int $file_id): array
    {
        $text_sort = $text_from_file->getText();
        $rows = [];
        $created_at = date('Y-m-d H:i:s');
        foreach ($text_sort as $word => $count) {
            $rows[] = [$created_at, $word, $count, $file_id];
        }

        return $rows;
    }
}