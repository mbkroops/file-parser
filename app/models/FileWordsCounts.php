<?php

namespace app\models;

use yii\db\ActiveRecord;

/**
 * This is the model class for table "file_words_counts".
 *
 * @property int $id
 * @property string|null $created_at
 * @property string|null $word
 * @property int|null $count
 * @property int|null $file_id
 *
 * @property FileInfo $file
 */
class FileWordsCounts extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'file_words_counts';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['created_at'], 'safe'],
            [['count', 'file_id'], 'integer'],
            [['word'], 'string', 'max' => 50],
            [['file_id'], 'exist', 'skipOnError' => true, 'targetClass' => FileInfo::className(), 'targetAttribute' => ['file_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'Идентификатор',
            'created_at' => 'Время создания',
            'word' => 'Слово',
            'count' => 'Количество в файле',
            'file_id' => 'Идентификатор файла',
        ];
    }

    /**
     * Gets query for [[File]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getFile()
    {
        return $this->hasOne(FileInfo::className(), ['id' => 'file_id']);
    }

    public function attributes(bool $skip_id = false)
    {
        $attributes = parent::attributes();
        if ($skip_id) {
            unset($attributes[0]);
        }

        return $attributes;
    }
}