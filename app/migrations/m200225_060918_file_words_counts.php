<?php

use yii\db\Migration;

/**
 * Class m200225_060918_file_words_counts
 */
class m200225_060918_file_words_counts extends Migration
{
    public function up()
    {
        $this->createTable('file_words_counts', [
            'id' => $this->primaryKey(),
            'created_at' => $this->dateTime(),
            'word' => $this->string(50),
            'count' => $this->integer(10),
            'file_id' => $this->smallInteger(8)
        ]);

        $this->alterColumn('file_words_counts', 'id',
            $this->smallInteger(8). ' NOT NULL AUTO_INCREMENT');

        $this->addForeignKey(
            'word_on_file',
            'file_words_counts',
            'file_id',
            'file_info',
            'id',
            'CASCADE'
        );
    }

    public function down()
    {
        $this->dropTable('file_words_counts');
    }
}
