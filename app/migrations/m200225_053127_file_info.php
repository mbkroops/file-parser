<?php

use yii\db\Migration;

/**
 * Class m200225_053127_file_info
 */
class m200225_053127_file_info extends Migration
{
    public function up()
    {
        $this->createTable('file_info', [
            'id' => $this->primaryKey(),
            'created_at' => $this->dateTime(),
            'file_name' => $this->string(50),
        ]);

        $this->alterColumn('file_info', 'id',
            $this->smallInteger(8). ' NOT NULL AUTO_INCREMENT');
    }

    public function down()
    {
        $this->dropTable('file_info');
    }
}
