<?php

namespace app\core\services;

/**
 * Клас обработки текста
 *
 * Class TextParse
 * @package app\core\services
 */
class TextParse
{
    private $text;

    public function __construct(string $text)
    {
        $this->text = $this->parse($text);
    }

    /**
     * Получение обработанного текста
     *
     * @return array
     */
    public function getText(): array
    {
        return $this->text;
    }

    /**
     * Парсим строчку и бьем ее на токены
     *
     * @param string $text
     * @return array
     */
    private function parse(string $text): array
    {
        $clear_text = preg_replace('/[^a-zA-Zа-яА-Я ]/ui', ' ', mb_strtolower($text));

        return array_count_values(array_filter(preg_split('/[\s,]+/', $clear_text)));
    }
}