<?php

namespace app\controllers;

use app\core\services\TextParse;
use app\models\FileInfo;
use app\models\LoginForm;
use app\models\UploadFileForm;
use Yii;
use yii\data\Pagination;
use yii\web\Controller;
use yii\web\Response;
use yii\web\UploadedFile;

class SiteController extends Controller
{
    /**
     * Домашняя страница.
     *
     * @return string
     * @throws \yii\base\InvalidArgumentException
     */
    public function actionIndex()
    {
        $model = new UploadFileForm();
        $words_table = [];
        if (Yii::$app->request->isPost) {
            $model->file = UploadedFile::getInstance($model, 'file');
            if ($model->file && $model->validate()) {
                $text_parse = new TextParse(file_get_contents($model->file->tempName));
                $file_info = new FileInfo();
                $data = $file_info->saveData($model->file->name, $text_parse)->getFileWordsCounts();
                $words_table = $data->asArray()->all();
            }
        }

        return $this->render('index.twig', ['model' => $model, 'words_table' => $words_table]);
    }

    /**
     * Страница авторизации.
     *
     * @return Response|string
     * @throws \yii\base\InvalidArgumentException
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login.twig', [
            'title' => 'Вход',
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Страница истории загрузок файлов.
     *
     * @return string
     * @throws \yii\base\InvalidArgumentException
     */
    public function actionHistory()
    {
        if (!Yii::$app->user->isGuest) {
            $query = FileInfo::find();
            $pagination = new Pagination(['totalCount' => $query->count(), 'pageSize' => FileInfo::PAGE_LIMIT]);
            $files = $query->offset($pagination->offset)->limit($pagination->limit)->all();
            $history['pagination'] = $pagination;
            foreach ($files as $file) {
                $words = $file->getFileWordsCounts()->asArray()->all();
                $history['content'][] = [
                    'created_at' => $file->getAttribute('created_at'),
                    'file_name' => $file->getAttribute('file_name'),
                    'words_table' => $words
                ];
            }

            return $this->render('history.twig', array_merge(['title' => 'История загрузок'], $history));
        }
    }

}
