FROM richarvey/nginx-php-fpm

ADD app /var/www/app
RUN rm -Rf /etc/nginx/sites-enabled/*
WORKDIR /var/www/app
