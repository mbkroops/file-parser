## Настройка и запуск окружения

- Клонирование проекта.
```bash
    git@gitlab.com:mbkroops/file-parser.git
```
Для локальной разработки требуется docker и docker-compose >= 1.25v.

- Чтобы запустить локально проект, следует использовать следующие команды:
```bash
    docker-compose up
    docker-compose exec app composer update
    docker-compose exec app php yii migrate
```
Сайт доступен по адресу http://localhost:9000

Вслучае возникокновени проблем с записью в нужные дериктории, просто выдайте права на запись для пользвоателя.